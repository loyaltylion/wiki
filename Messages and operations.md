# Synchronous operations

This is stuff that can't really be handled by a message queue, because we need to assure the sender of success immediately.

* Event creation (and customers, referrals, etc)
* *Most* activity creations. This includes things like purchases and reviews. Server-side events do not technically need to be synchronous, but if they aren't then notifications could be delayed for the user. Client events should always really be immediate, like getting points for posting a tweet.

# Asynchronous operations

* Creating side-effect activities (e.g. referral activity after a purchase). These don't need to be synchronous as they are not triggered directly by the owning customer (but by the action of a different customer).
* Approving activities after time elapsed

## Activity approval

One method of activity approval is to wait X days and then approve it (unless it has been manually declined prior to then).

Although the purest way to do this would probably be to use an MQ of some kind, I don't think we need that much bother right now.

I'm just going to set the approval time in the activity, and have a job that runs every so often (twice a day ought to be fine) and adjusts jobs that need adjusting.