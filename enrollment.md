# Member enrollment

By default customers do not participate in the loyalty programme. They have to confirm their desire by enrolling.

## On the merchant

A merchant needs to prompt their customers to enrol. The easiest way to do this is to have a page on the site, visible to logged in customers, which explains the loyalty programme and has a nice big button they can click to sign up.

Existing customers at the time LL is installed can be emailed a link to this, so they can get started. New customers who sign up after LL can be redirected to this page after sign up.

It's also possible to prompt customers to join in other places. It might be sensible to have an 'easy enrol' place in the checkout process, which would say something like "if you join our loyalty programme you'll get x points for this purchase".

## On our side

Customers in our DB have an `enrolled` flag, which defaults to false. When this is false, any activities completed by this customer will not reward any points.

The actual enrol mechanism is performed through the XDM endpoint. The merchant needs to add a `data-lion-enrol` attribute to a button and the SDK will add a listener to it. When pressed, we'll send an enrol message through XDM and update their account.

A customer must be authenticated with LL before they can enrol/unenrol, so there is a potential problem if the auth call doesn't finish before they click the 'Enrol' button.

This is most likely to occur when a customer is already logged in when LL is installed. If the first page they hit is the 'Join the loyalty program' page, this will also be the first opportunity the SDK has to auth them, which will do immediately but could take a second or more.

So, when the 'Enrol' button is clicked, we'll check if the customer is still authenticating and, if so, we'll wait and try again periodically.

## After enrolling

After the customer has clicked the 'Enrol' button, and our server has registered the change, the SDK should redirect the customer to their account page (so we'll need the merchant to tell us where this lives, if it has a non-standard url).

## UI differences between enrolled/non-enrolled customers

...










