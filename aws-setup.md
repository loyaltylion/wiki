
# EC2 instances

## Elastic IPs

1. **web server:**
2. **staging server:** ec2-54-228-181-220.eu-west-1.compute.amazonaws.com

## knife ec2 server create

    knife ec2 server create --image ami-02f4fe76 --flavor m1.small --region 'eu-west-1' --groups webserver --distro 'ubuntu12.04-gems' --ssh-user ubuntu --node-name 'name' --run-list "role[base],role[web_server]"

## run list for staging server

    role[base],role[app_server],role[mongo_server],role[redis_server],role[web_server],role[postgresql_server]


# On VPC (not using)

EC2s live in a VPC so they can have persistent IP addresses

* Subnet ID: subnet-d20150ba

## VPC security groups

* Web server: web-server-sg
* App server: app-server-sg

## create ec2 instance

Unfortunately the `knife ec2 server create` command seems to be bugged and does not allow placing a new instance into a VPS security group, so we have to create the EC2 instances and then bootstrap them with knife.

## bootstrap ec2 instance with chef



## use knife ec2 to create ec2 instances

    knife ec2 server create --image ami-02f4fe76 --flavor m1.small --region 'eu-west-1' --subnet 'subnet-d20150ba' --security-group-ids webserversg --distro 'ubuntu12.04-gems' --ssh-user ubuntu --run-list "role[base],role[web_server]"