## Some handy notes on this stuff

## index mappings

elasticsearch creates index mappings for each day of logstash logs. it creates the mapping for each property based on the first data it gets for that property

this can lead to some real headaches if a later log on the same day sends a property in a format that does not align with the index mapping ES has set, e.g. if you try to send "false" to a property that previously was sent a date "2013-01-01". this will break, because ES has a type: date, and will reject false

when this happens there will be an unholy amount of logspam, in the region of 100MBs or even GBs, as logstash continually tries to send the log to ES and ES rejects it over and over

you can see a hash of all the mappings for each property like this:

    curl -XGET 'http://localhost:9200/logstash-2013.11.06/_mapping'
    
which will help diagnose what's going on!

### so what if I did mess up the mapping?

yeah OK, if you did this then that was a bad move. It's kind of fucked now. You can delete the entire day's mapping & index for a quick fix but that will lose all the data too.

a longer fix would involve reindexing the stuff somehow . . . .