# MongoDB

Used to store events, customer data, referrals, etc. One database per environment (for now, probably should be separated eventually?)

Database is like so:

## events.x

All tracked events for a given site live here, e.g. **events.10**, *events.23*. One events collection per Site, suffixed with the site ID. This avoids having billions and billions of documents in a single events collection.

## customers

All customers who have been identified (and potentially authenticated) with Prizgo. Each customer belongs to a Site (referenced by site_id)

## sites

This is a MongoDB cache of the sites from Postgres (the main application db), which avoids having to keep a connection to both Mongo & Postgres in our node apps. The prizgo web app is responsible for ensuring this is kept up to date.

## referrals

All generated referrals live here, linked to a site and customer

