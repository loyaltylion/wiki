# Aggregate stuff

## total points given out (by site)

    db.customers.aggregate({
      $group: {
        _id: '$site_id',
        approved: { $sum: '$points.approved' },
        pending: { $sum: '$points.pending' }
      }
    })

## total points spent (by site)

    

## total points for a site

    db.customers.aggregate({
      $match: {
        site_id: ObjectId('518a27ca5d0f052e4300000c')
      }
    }, {
      $group: {
        _id: '$site_id',
        approved: { $sum: '$points.approved' },
        pending: { $sum: '$points.pending' }
      }
    })

## find activities for purpleleaves

    db.activities.find({site_id:ObjectId('518a27ca5d0f052e4300000c')})

## purchase activities for PL

    db.customers.find({
      activities: {
        $elemMatch: {
          activity_id: ObjectId('518a28df5d0f052e4300000f')
        }
      }
    }

## number of activities completed for site

    db.customers.aggregate({
      $match: {
        site_id: ObjectId('518a27ca5d0f052e4300000c')
      }
    }, {
      $unwind: "$activities"
    }, {
      $group: {
        _id: '$site_id',
        count: { $sum: 1 }
      }
    })
    
    db.customers.aggregate({
      $unwind: "$activities"
    }, {
      $group: {
        _id: '$activities.activity_id',
        count: { $sum: 1 }
      }
    })

## number of activities completed for site, grouped by activity_id

    db.customers.aggregate({
      $match: {
        site_id: ObjectId('518a27ca5d0f052e4300000c')
      }
    }, {
      $unwind: "$activities"
    }, {
      $group: {
        _id: '$activities.activity_id',
        count: { $sum: 1 }
      }
    })
    
### ...with total value

    db.customers.aggregate({
      $match: {
        site_id: ObjectId('518a27ca5d0f052e4300000c')
      }
    }, {
      $unwind: "$activities"
    }, {
      $group: {
        _id: '$activities.activity_id',
        count: { $sum: 1 },
        total_value: { $sum: '$activities.value' }
      }
    })

## show customers who were referred

    db.customers.find({ referred_by: { $ne: null } })

## referrals with hits for PL

    db.referrals.find({
      site_id: ObjectId('518a27ca5d0f052e4300000c'),
      hits_n: { $gt: 0 }
    })

## customer emails with points, ordered

    db.customers.find({
      site_id: ObjectId('518a27ca5d0f052e4300000c')
    }, { email: 1, 'points': 1
    }).sort({ 'points.approved': -1 })
      

## customers who have rewards

    db.customers.find({
      site_id: ObjectId('518a27ca5d0f052e4300000c'),
      rewards: { $ne: [] }
    })

## event counts by type

    db.events_518a27ca5d0f052e4300000c.aggregate({
      $group: {
        _id: '$name',
        count: { $sum: 1 }
      }
    })

## signups by day

    db.events_518a27ca5d0f052e4300000c.aggregate([
      {
        $match: {
          name: '$signup'
        }
      },
      { 
        $group: {
          _id: {
            y: { $year: '$time' },
            m: { $month: '$time' },
            d: { $dayOfMonth: '$time' }
          },
          count: { $sum: 1 }
        }
      },
      {
        $sort: { '_id.y': 1, '_id.m': 1, '_id.d': 1 }
      }
    ])

## total purchase revenue

    db.events_518a27ca5d0f052e4300000c.aggregate([
      { $match: {
        name: '$purchase'
      } },
      { $group: {
        _id: 'purchase',
        total: { $sum: '$properties.total' }
      } }
    ])











