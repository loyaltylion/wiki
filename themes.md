Themes dictate the look and feel of LL components, including:

* The referral popup
* The account pages

The simplest way for a theme to change the look is to add some custom CSS, but we'll also support custom templates for certain elements.

## Technical stuff

CSS is easy: we just inject it onto the page if we have any.

Templates are more involved. We should try to keep each 'component' clean with its own template (currently dustjs). We render account pages using Backbone, which uses the dustjs templates for each 'component', but these dustjs templates can also be used to render the page server-side.