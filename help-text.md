## dutch

    # Wat is dit?
    
    Dit is onze manier om onze waardering te uiten. Je kan punten verdienen voor verschillende activiteiten op onze website, zoals doorverwijzingen en aankopen die je hebt gedaan. Je kan die punten gebruiken voor kortingen op je aankopen, dus hoe meer punten je verzamelt hoe meer geld je bespaart.
    
    # Wie kan er meedoen?
    
    Iedereen met een account is automatisch ingeschreven.
    
    # Hoe verdien ik punten?
    
    Je kan punten verdienen door verschillende dingen te doen, inclusief het doorverwijzen van vrienden, en het doen van aankopen. Om alle manieren waarmee je punten kan verdienen te zien klik je op de 'Verdien punten' tab in het menu.
    
    # Hoe kan ik mijn puntenbalans zien?
    
    Je puntenbalans zie je op iedere pagina in de bovenste balk.
    
    # Hoe kan ik punten inruilen?
    
    Selecteer de tab genaamd 'Punten inruilen'. Hier kan je alle beloningen die we aanbieden zien. Als je genoeg punten hebt kan je ze inruilen voor een beloning.
    
    # Is er een limiet aan het aantal punten dat ik kan verdienen?
    
    Nee. Verdien er zoveel als je wilt.
    
    # Kunnen mijn punten verlopen?
    
    Ja. Als je geen punten verdient of gebruikt binnen 12 maanden verlopen ze.
    
    # Wat betekent het als een status op 'Goedgekeurd', 'Wachtend' of 'Afgewezen' staat?
    
    * Goedgekeurd: Deze punten kunnen meteen worden ingeruild voor beloningen
    * Wachtend: Deze punten moet nog geverifieerd worden voordat je ze kan inruilen. Dit geldt voornamelijk voor doorverwijzingen en aankopen.
    * Afgewezen: Deze punten zullen niet worden toegevoegd aan je account. Dit gebeurt bijvoorbeeld wanneer je een aankoop annuleert (de punten zullen dan veranderen van ‘wachtend’ naar ‘afgewezen’)
    
    # Wat gebeurt er als een vriend die ik heb doorverwezen zijn bestelling annuleert of terugstuurt?
    
    Je bestedingspunten worden geannuleerd en zullen worden verwijderd van je account.
    
    # Waarom is de balans op mijn account omlaag gegaan?
    
    Jij of iemand die je hebt doorverwezen, heeft een aankoop geannuleerd of teruggebracht.
    
    # Ik heb een activiteit voltooid maar geen punten verdiend.
    
    Het kan soms een aantal minuten duren voordat wij je activiteiten hebben kunnen verwerken en de punten kunnen bijschrijven.
    
    # Kan ik mijn punten gebruiken bij de betaling?
    
    Niet direct – lever eerst je punten in voor een voucher die dan weer kan worden gebruikt bij de betaling.
    
    # Hoe kan ik me uitschrijven uit het programma?
    
    Als je niet langer punten wilt verdienen kan je contact met ons opnemen en om een uitschrijving vragen. Met je uitschrijving zal je alle punten verliezen die je hebt opgespaard.
    
    # Wat gebeurt er als ik me uitschrijf en later besluit me weer aan te melden?
    
    Neem gewoon contact met ons op en we zullen je opnieuw inschrijven. Je totaal aantal punten zal dan wel vanaf nul beginnen.
    
    # Waar kan ik een probleem melden of feedback geven?
    
    Je kan daarvoor het normale contactformulier gebruiken.

# english

    # What is this?

    This is our way of showing our appreciation. You’ll earn points for activities on our site, like referrals and purchases. You can use them to earn discounts off purchases, so the more you collect the more you save.
    
    # Who can join?
    
    Anyone with an account is automatically enrolled.
    
    # Does it cost me anything?
    
    No. It's completely free!
    
    # How do I earn points?
    
    You can earn points for all sorts of activities, including referring friends, and making purchases. To see all the ways you can earn points click the ‘Earn Points’ tab in the menu.
    
    # How do I view my point balance?
    
    Your point balance is on every page in the top bar, and is also visible on the *Account* tab.
    
    # How do I redeem my points?
    
    Select the tab called *Redeem*. Here you’ll see all the rewards we offer. If you have enough points, you can redeem them for a reward.
    
    # Is there a limit to the number of points I can earn?
    
    No. Go ahead and earn as many as you can!
    
    # Do my points expire?
    
    Yes. If you don't earn or spend any points in 12 months they'll expire. Don't worry - we'll contact you before this happens!
    
    # What do the 'approved', 'pending' and 'cancelled' statuses mean?
    
    * **Approved:** These points can be redeemed on rewards immediately
    * **Pending:** These points are need to be verified before you can redeem them. This typically applies to purchases and referrals
    * **Cancelled:** These points will not be added to your account. For example, this will happen if you cancel a purchase (the points will change from pending to cancelled)
    
    # What happens if a friend I refer cancels or returns their order?
    
    Your pending points will become cancelled and will be removed from your account.
    
    # Why did my account balance go down?
    
    You, or someone you referred, cancelled or returned a purchase.
    
    # I completed an activity but didn't earn points!
    
    It can sometimes take a few minutes for us to process your activity and provide your points.
    
    # Can I use my points during checkout?
    
    Not directly - please redeem your points for a voucher which can then be applied during checkout.
    
    # How do I leave the program?
    
    If you no longer wish to earn points, please contact us and ask to be unenrolled. We'll unenroll and you will lose any points you have accrued.
    
    # What happens if I leave and decide to join again?
    
    That's no problem! Just contact us and we'll re-enroll you. However, your point total will begin from zero.
    
    # Where can I report a problem or give feedback?
    
    Please use the contact link at the bottom of the page.
