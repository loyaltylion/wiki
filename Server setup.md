Create Linode: location London, build Ubuntu 12.04 64bit

* Put root password in spreadsheet
* Set name of Linode (should match hostname) and group
* Add private IP (remote access tab)
* Boot Linode and ssh in as root

Set hostname

    $ echo "app1" > /etc/hostname
    $ hostname -F /etc/hostname

Add A record for FQDN if this is a web-accessible server

    e.g. app1.prizgo.com > 152.49.32.52

Set timezone to UTC

    $ dpkg-reconfigure tzdata

set up static networking

* http://library.linode.com/networking/configuring-static-ip-interfaces#sph_debian-ubuntu

update /etc/hosts with fqdn for private ip:

    <private_ip>   app1.loyaltylion.com app1

bootstrap with chef

    $ knife bootstrap $ip --ssh-user root --ssh-password $pw --environment prod --run-list "role[base],role[app_server]" --distro "ubuntu12.04-gems"

**reboot**