This page explains how we handle languages, translations and customisations!

It applies to every customer-facing part of LoyaltyLion, i.e. all the widgets, emails sent, etc. It does **not** apply to the language of loyaltylion.com -- that is very low priority right now and will be done using the normal Rails i18n support.

However, because LL has a lot of customer-facing stuff, that exists on our merchant's stores, the internationalisation of this stuff is a lot more important (otherwise we could only really have English stores)

# Languages

LoyaltyLion is offered in a growing number of languages, which can be thought of as 'base' languages. These are languages that we support and have a base translation for, i.e. we have a translation for every string in every widget, email, etc

## Multiple languages

When a merchant creates their LL program they select a 'primary' language. If they are a mono-language store, that's pretty much all they need to do.

However, if their store is offered in multiple languages, they can choose additional languages (auxilary languages) as well. 

This complicates things for them a little bit, as they will now need to supply any customer-facing text in all selected languages. E.g. when they create a new activity/rule, they will have to give it a title and description in all selected languages.

## Modifying languages

Although we supply a base translation for every language, for some merchants this translation is not suitable - they may want to change the tone, meaning, etc.

So, we allow a merchant to adjust their chosen language translations. This is not particularly elegant - they get a list of all the words and phrases and a textbox for each where they can enter a custom translation.

When we get the translations inside any customer-facing rendering, we merge any custom translations with the original (base) translations to create the true translations object. this is then used for all customer-facing output.

This is quite a low-level feature and probably will not be used by smaller stores. It would, for example, let you change any reference to 'points' into 'stars'.

It is **separate** from the *customisations* functionality (explained below)

# Customisations

Customisations allow two things:

- adding custom css, html, etc
- changing text in pre-defined areas

As an example of the latter, in the referral widget, the title and summary are customisable elements. Everything else in the widget will be drawn from the language translation (or any overrides if they exist).

The reason that some text is considered a 'customisation' is because these are considered 'hotspots' for changing, and thus are given special consideration in the UI, i.e. they don't require going to the 'low level' translations override feature to change and have a nicer interface to edit them.

Textual customisations and translations are mutually exclusive, that is to say translations do not exist for any text that is marked as a customisation. When a new site is created, textual customisations are created for that language using the language defaults. It seems simpler this way, instead of having these strings duplicated in customisations and translations which could get confusing.





