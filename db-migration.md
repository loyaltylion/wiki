# db migration

## create pl user

    pl = User.create :email => 'kleinschmidt@purpleleaves.de', :first_name => 'Thomas', :last_name => 'Kleinschmidt', :password => 'purpleleaves'

## create pl production site

    pl.sites.create :name => 'purpleleaves (production)', :domain => 'www.purpleleaves.de', :token => '0fb99783b1af80308d662f047d627367', :secret => 'fe18bd5d354b91a42d095041eb7d7a3f', :settings => '{ "referrals" : { "headline" : "Spread the word and earn rewards", "message" : "Share your unique link and get a 1000 points every time you refer us a new customer who makes a purchase", "email_subject" : "Purple leaves sell awesome crowd-designed t-shirts", "default_email_message" : "Hi,\r\n\r\nI thought you might be interested in purple leaves - they sell awesome crowd-designed t-shirts.", "default_tweet" : "purple leaves sell lots of awesome, crowd-designed t-shirts. Check it out here" } }'

## mongodb backup

not actually needed, but useful for testing -- migration just uses the main mongo db

    mongodump --host 192.168.10.183 --db main --out mongo-2013-07-09/

