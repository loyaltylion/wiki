# Create site at prizgo.com
Can have as many sites as you want, but data, points, etc are not shared between them. Usually a good idea to create one site for testing.

Each site has a token (public) and secret (private) which will be used to identify and sign requests to Prizgo.

# Load Javascript SDK on every page

```
#!html
<script>
  (function(t,e){window.prizgo=e;var r,i=t.getElementsByTagName("script")[0];r=t.createElement("script"),r.type="text/javascript",r.async=!0,r.src="http://cdn.prizgo.dev/prizgo.js",i.parentNode.insertBefore(r,i),e.init=function(t){function r(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}}for(var i="track_pageview track_activity track_referral identify_customer identify_product auth_customer".split(" "),n=0;i.length>n;n++)r(e,i[n]);e._token=t}})(document,window.prizgo||[]);

  prizgo.init('your_site_token');
</script>
```

This will (asynchronously) initialise Prizgo. Anywhere below this snippet you can (immediately) interact with Prizgo via the `prizgo` object. Calls to the `prizgo` object will be buffered and executed once the SDK has fully initialised.

# Setting up Single Sign On
Although customers never see (or log in to) Prizgo, we need them to be authenticated with us so we can securely show them their loyalty information and let them redeem points. We do this using a simple variation of Single Sign On (SSO).

This involves you setting up a URL - usually /loyalty/authenticate, but it can be anything (e.g. /loyalty/authenticate.php). 

There are three things you need to do when one of your customers accesses this URL:

## Send a POST request to api.prizgo.com/customers/authenticate
This should have a `user_id` parameter set to the ID of the current logged in customer.

