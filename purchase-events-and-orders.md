# Purchase events

Purchase events (events with name $purchase) are special, and can't really be treated the same as other events. This is because a purchase is directly related to an **Order**, which is a complex entity that goes through numerous states, can be paid, cancelled, returned, have items added or removed, etc. Not to mention it has a total attached to it, which can also change, and will include tax, shipping, discounts, etc

Basically, purchases/orders are a complicated thing and the standard "track event, maybe cancel or approve it later" just doesn't work for them without support

## Orders

So we have the orders table. In here we store all the orders we've been told about, including their current data/state, and any data/state they had before.

not only is this necessary for any kind of purchase analytics, but it helps us figure out when to give out points for an order, and how many, and if they are allowed to be approved (i.e. are they paid?)

    Orders:
      id: integer
      site_id: integer
      customer_id: integer
      
      # the total is the absolute total, which should include any tax,
      # shipping and discounts. 
      total: integer
      total_tax: integer
      total_shipping: integer
      total_discounts: integer
      discount_codes_used: string[]
      
      # we'll only ever allow a customeractivity linked to this order
      # to be approved if this is true
      paid: boolean
      
      # if we have the products involved in this order in our DB, their
      # ids will be stored here
      product_ids: integer[]
      
      # this is the current state of the order. it's a string, and its
      # actual value will depend on the platform, as each platform has 
      # their own states/statuses
      state: string
      
      # these are our own internal timestamps
      created_at: timestamp
      updated_at: timestamp
      
      # these are all for storing merchant data
      merchant_id: string
      merchant_number: string
      merchant_created_at: timestamp
      merchant_updated_at: timestamp
      merchant_product_ids: string[]
      
      # history is a json array of objects, each with a 
      # timestamp and the above fields, for history usage
      history: json


## So how does this work?

Ultimately, we care about only two things regarding an order:

* Has it been paid for?
* Has a refund been provided?

When an order has been paid for, we will either approve the points associated with the order, or start the 'pending period', the optional grace period (e.g. 14 days) to allow for returns. Put simply, redeemable points will never be granted until we're told payment has been taken.

We also need to know if a refund has been given. Refunds can happen for various reasons - returns, mistaken orders, unfulfillment issues, etc. The reason is not really important, all we care about is that a refund has taken place, and the amount that was refunded.

We'll use this refund amount to deduct the appropriate amount of points from the associated CustomerActivity. If the points are still pending, this is simple and will have no side-effects. If the points are approved and have already been spent, this is a bit more complicated as it will involve adding a debt to the customer in question. This debt will need to be repaid from future earned points before they can redeem

### Tracking a purchase (order creation)

When a $purchase is tracked, a new Order will be created. The order's details will be pulled from the purchase event properties, e.g. the state, totals, products, etc. The order's `event_id` will be set to the $purchase event.

If applicable, a CustomerActivity will then get created as usual. It will only be approved, or have a queued adjustment made, if the order is paid. The idea here is, points should never be given until the order has been paid for.

Referrals will also follow the same logic, so a referrer will only get points when their referee actually pays for their purchase.

### Updating a purchase (order update)

Orders typically undergo a lot of state changes. So we allow for updates to be applied to Orders.

When an update comes in, we look for an order with the associated `merchant_id`. If found, it will then be updated. This may include adjusting the price, changing the paid flag, changing the state, changing the products involved, etc.

Depending on what the change was to the `paid` flag and the `state`, we'll then adjust any associated activities (through the `event_id`).

So if an update comes in which changes the state of an order and marks it as being paid, we'll then look for a CustomerActivity connected to this order, and set either:

* approve it immediately, if there is no wait period
* create a queued adjustment to approve it after the wait period

If an update comes in which changes the state of an order to cancelled or returned, we'll adjust any connected CustomerActivity accordingly, by cancelling it, which will set the points to declined or void.


## Shopify orders

Shopify maintains two statuses for orders:

* Payment status (financial_status in api)
* Fulfillment status

**Payment status** can be:

* pending: The finances are pending.
* authorized: The finances have been authorized.
* partially_paid: The finances have been partially paid.
* paid: The finances have been paid.
* partially_refunded: The finances have been partially refunded.
* refunded: The finances have been refunded.
* voided: The finances have been voided.

Payments are first authorised and then acquired -- but there can be a wait in between these stages. And, in fact, the amount acquired can end up being less than the amount that is authorised.

Refunds can also happen for either the whole amount, or a partial amount. Shopify's return/refund support is actually very poor, and is mostly done by hand. There is no such thing as a "return" - instead you just manually note down that part of an order was returned, and then calculate and refund the amount yourself.

**Fulfillment status** can be:

* null: not fulfilled at all
* partial: partially fulfilled
* fulfilled: completely fulfilled


## SEOshop orders

Seoshop orders are pretty granular, and have 3 statuses: a 'general' status, a payment status and a shipment (fulfillment) status.

**General status** can be:

* offer: not a real order yet
* awaiting_payment: an order that has not been fully paid for
* awaiting_shipment: the order has been fully paid for
* shipped: order has been fully shipped

**Payment status**:

* not_paid: not paid for at all
* partially_paid: partially paid for
* paid: fully paid for

**Shipment status**:

* not_shipped: no items shipped
* partially_shipped: partially shipped
* shipped: all items shipped

SEOshop has fairly robust returns handling. You can create a return for a particular item and adjust the state of that return.

However, refunds are another matter. You can mark a return as 'refunded', and it will show as being refunded, but the paymentStatus of the order will NOT change (like it does in Shopify).

Getting the list of products in an order via the API will show a `quantityReturned` field, which will show the quantity of that product which has been returned. There is a `quantityRefunded` field here too, but I have never seen that show anything other than 0, despite having set a return to `refunded` in the backoffice.

So the upshot of this is currently there is no way to know, automatically, when an order has been partially or fully refunded, so the merchant has to inform us of a refund so we can adjust the points in this instance.

### Payments

Payment status is not as granular as Shopify. There is a 

## Order state spec

Because there's so many damn things that can happen with an order, I'm writing down everything I can think of here.

### Changes prior to payment

Order placed, not yet paid. An item is removed or added to the order, thus changing the totals.

Order placed, not yet paid. A discount code is applied to the order, thus changing the totals and codes used.

Order placed, not yet paid. A discount code is removed from the order, thus changing the totals and codes used.

Order placed, not yet paid. Entire order is cancelled.

Order placed, not yet paid. Order is paid for.

### Changes after payment

Order paid for. An item is removed from the order and a refund is given. This occurs BEFORE shipping

Order paid for. Entire order is cancelled BEFORE shipping. Entire amount is refunded.

Order paid for. Discount code is applied BEFORE shipping and a pro-rata refund given. This seems very rare and most platforms won't support doing this.

Order paid for. Payment goes through but is later voided (e.g. chargeback?)

### Changes after shipping/fulfillment

Order is shipped. Entire order is returned and refund provided.

Order is shipped. Part of order is returned and refund provided pro-rata.

Order is shipped. Entire order is returned but return is rejected - no refund given.

Order is shipped. Part of order is returned but return is rejected - no refund given.

Order is shipped. Product is 


















