to take a DB dump from production

after sshing in

    $ sudo -u postgres pg_dump --no-owner -Fc lion_prod > lion_prod_`date '+%s'`.dump
    

to restore it locally (change owner and name as needed)

    $ createdb -O loyaltylion -T template0 lion_dev_fromlive_09_25
    $ pg_restore -d lion_dev_fromlive_09_25 lion_prod_1380085615.dump

you might get some permission errors but they can probably be ignored