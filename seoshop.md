# SEOshop

## Partner account

There seem to be two partner systems, a test one (seoshop.webappshop.net) and a live one (seoshop.webappshop.com)

The test one goes down quite a lot, so use the live one unless told otherwise

### Live server

* URL: https://seoshop.webshopapp.com/partners
* email: dave@prizgo.com
* pass: 44bae

## Test shops

To access test shops, first log in to the backoffice using the above username/password, at this url: https://seoshop.webshopapp.com

Then in the bottom left in the menu you'll see a "Shop" heading and a dropdown. You can select which shop to work on here. 

Under the "Partner > Shops" section you can also create new shops, which is necessary because any "demo" stores created expire after 14 days. Haha. Fun.

### ll-dev

* URL: http://ll-dev-24775.webshopapp.com
* Env: dev

### ll-live-one

* URL: http://ll-live-one.webshopapp.com
* Env: prod

## Orders / offers

In SEOshop you can create an order manually. This isn't as straightforward as it sounds...

When you first create an order it starts as an **offer**. Products can be added to it and the price is in flux, until it is converted to an order.

Unfortunately, it's represented as an order internally, and when created fires an orders/created webhook. This webhook is useless, because the order has no price/products and is subject to change.

So what we do is check the status, which will be `offer`, and skip it if so. When the order is eventually turned into an `order`, the status will no longer be `offer`, and an orders/updated webhook will be fired.

The API needs to handle orders/updated and allow them to (somewhat counter-intuitively) create a new $purchase.

## Offer

    {
      "order": {
        "id": 1563355,
        "createdAt": "2013-11-20T07:29:15+01:00",
        "updatedAt": "2013-11-20T07:29:35+01:00",
        "number": "",
        "status": "offer",
        "channel": null,
        "remoteIp": false,
        "userAgent": false,
        "referralId": false,
        "priceCost": 0,
        "priceExcl": 28.9222,
        "priceIncl": 28.9222,
        "weight": 0,
        "volume": 0,
        "colli": 1,
        "gender": false,
        "birthDate": false,
        "nationalId": "",
        "email": "tilly.jones@zozi.eu",
        "firstname": "Tilly",
        "middlename": "",
        "lastname": "Jones",
        "phone": "",
        "mobile": "",
        "isCompany": false,
        "companyName": "",
        "companyCoCNumber": "",
        "companyVatNumber": "",
        "addressBillingName": "Tilly Jones",
        "addressBillingStreet": "",
        "addressBillingStreet2": "",
        "addressBillingNumber": "",
        "addressBillingExtension": "",
        "addressBillingZipcode": "",
        "addressBillingCity": "",
        "addressBillingRegion": "",
        "addressBillingCountry": false,
        "addressShippingCompany": false,
        "addressShippingName": "Tilly Jones",
        "addressShippingStreet": "",
        "addressShippingStreet2": "",
        "addressShippingNumber": "",
        "addressShippingExtension": "",
        "addressShippingZipcode": "",
        "addressShippingCity": "",
        "addressShippingRegion": "",
        "addressShippingCountry": false,
        "paymentId": "banktransfer",
        "paymentStatus": "not_paid",
        "paymentIsPost": false,
        "paymentIsInvoiceExternal": false,
        "paymentTaxRate": 0,
        "paymentBasePriceExcl": 0,
        "paymentBasePriceIncl": 0,
        "paymentPriceExcl": 0,
        "paymentPriceIncl": 0,
        "paymentTitle": "Advance payment via bank transfer",
        "shipmentId": "40425",
        "shipmentStatus": "not_shipped",
        "shipmentIsCashOnDelivery": false,
        "shipmentIsPickup": false,
        "shipmentTaxRate": 0,
        "shipmentBasePriceExcl": 4.1322,
        "shipmentBasePriceIncl": 4.1322,
        "shipmentPriceExcl": 4.13,
        "shipmentPriceIncl": 4.13,
        "shipmentDiscountExcl": 0,
        "shipmentDiscountIncl": 0,
        "shipmentTitle": "UPS",
        "isDiscounted": false,
        "discountType": "amount",
        "discountAmount": 0,
        "discountPercentage": 0,
        "discountCouponCode": "",
        "comment": "",
        "memo": "",
        "doNotifyNew": false,
        "doNotifyReminder": false,
        "doNotifyCancelled": false,
        "language": {
          "id": 3,
          "code": "en",
          "locale": "en_US",
          "title": "English"
        },
        "customer": {
          "resource": {
            "id": 1378782,
            "url": "customers\/1378782",
            "link": "https:\/\/api.webshopapp.com\/en\/customers\/1378782.json"
          }
        },
        "invoices": {
          "resource": {
            "id": false,
            "url": "invoices?order=1563355",
            "link": "https:\/\/api.webshopapp.com\/en\/invoices.json?order=1563355"
          }
        },
        "shipments": {
          "resource": {
            "id": false,
            "url": "shipments?order=1563355",
            "link": "https:\/\/api.webshopapp.com\/en\/shipments.json?order=1563355"
          }
        },
        "products": {
          "resource": {
            "id": false,
            "url": "orders\/1563355\/products",
            "link": "https:\/\/api.webshopapp.com\/en\/orders\/1563355\/products.json"
          }
        },
        "metafields": {
          "resource": {
            "id": false,
            "url": "orders\/1563355\/metafields",
            "link": "https:\/\/api.webshopapp.com\/en\/orders\/1563355\/metafields.json"
          }
        },
        "events": {
          "resource": {
            "id": false,
            "url": "orders\/events?order=1563355",
            "link": "https:\/\/api.webshopapp.com\/en\/orders\/events.json?order=1563355"
          }
        }
      }
    }













