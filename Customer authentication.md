# Customer authentication overview

Every site customer has an "account" with LL, though they never interact with this directly, and it has no username or password. We use something resembling the single sign-on pattern to sign customers into their LL account securely, and without interrupting their user flow.

## Overview

On each page load on the merchant's site, the SDK connects with a remote XDM endpoint on `platform.loyaltylion.com` which has a third party cookie. If a customer is authenticated with us, the XDM endpoint will reply with their customer data, and the SDK will know it can do secure customer things.

If the customer is *not* authenticated, this endpoint will return no customer data. In this case, one of two things should happen:

1. If the customer is logged in to the merchant, we should try to auth them with us
2. If customer is not logged in, we do nothing

The first thing is the one we care about. This will typically occur in two situations:

1. Customer just logged in to the merchant
2. Customer was already logged in to merchant *before* LL was installed

## Implementation

In the situation where a customer is `identified` (i.e. logged in to the merchant) but not `authenticated` (has a session with us), the JS SDK will attempt to authenticate them.

### Merchant

This requires one thing from the merchant, which is an HTTP endpoint which will contact the LL API to get customer auth tokens.

The endpoint should respond to POST requests on (default) the url `/loyalty/authenticate`. When called, which will be via an XHR in the JS SDK, it should:

1. Send a request `/customers/authenticate` to the LL API with the ID of the current logged in user (there is a convenience function for this in the client libraries) 
2. Return a JSON body:

        {
          "auth_token": "......"
        }

If anything goes wrong (e.g. no customer is actually logged in, the API timed out, etc), any 4xx error could should be returned.

### LoyaltyLion

After the XDM endpoint has finished initialising, and has told us no customer is authenticated, the SDK will proceed to send an XHR to the endpoint on the merchant. Hopefully, this replies with a new auth token. Now a final request is sent back to `platform.loyaltylion.com`, using XDM, which will verify the auth token and authenticate the customer. Subsequent requests from this browser will now have this customer authenticated with LL.

## Security ##

Each auth token is valid for a single authentication, and is then discarded, preventing replay attacks.

The weak link is the XHR to the merchant's `/loyalty/authenticate`. If this is not SSL, then someone could watch the traffic and steal the returned auth_token, using it to authenticate as the original customer.

This would then allow the attacker to access any of the secure pages on `platform.loyaltylion.com` and thus spend a user's points.

### Mitigations ###

**Verify identity:**
In cases where a customer is known to us (e.g. they have been submitting JS events) we will know things like their IP address and user agent. It is a good assumption that an authentication request for this customer will originate from the same IP address & user agent (unless some time has elapsed since the last event we tracked from them and the auth request)

So, let's say customer 'xyz' sends a few JS events tracking some page views, and has ip address 5.1.1.1 and user agent 'firefox 18.4',

We get an authentication request for an auth_token connected to the customer 'xyz', but we check the request headers and the user agent is 'chrome 20.1' -- hmm, suspicious! we flag this as a potential attack, void the auth token and do something (to the user, on their next page load, we could check if they have switched to using chrome all of a sudden, and if not, show a warning and lock down their account for a time)

A truly dedicated hacker could bypass this, of course, but, honestly, by that point they will instead have simply cloned the user's actual session to the retail site and have full access to their whole account (a much bigger prize).
