# General info

## VPC Subnets

### Public

* subnet-8d035ae5 - eu-west-1a
* subnet-98055cf0 - eu-west-1b
* subnet-62045d0a - eu-west-1c

### Private

* subnet-c3055cab - eu-west-1a
* subnet-7f045d17 - eu-west-1b
* subnet-42045d2a - eu-west-1c

## AMIs

* Ubuntu 12.04 x64 (EBS, Parav): **ami-ce7b6fba**
* Ubuntu 12.04 x64 (EPH, Parav): **ami-fc7a6e88**
* Ubuntu 12.04 x64 (EBS, HVM): **ami-03d80d74**
* Ubuntu 12.04 x64 (EPH, HVM): **ami-39c5134e**

## Security groups

* External SSH (22 in): sg-9ae304f5
* Internal SSH: sg-12bd5a7d
* Base (nagios support etc): sg-c9ba59a6
* Web server (80/443 in): sg-7e907711
* Postgres server: sg-66907709

# Creating instances

## web server

    knife bootstrap 192.168.0.13 --ssh-gateway zoz@tools.loyaltylion.com --sudo --ssh-user ubuntu --identity-file ~/.ssh/dave-chef-ec2.pem --environment production --node-name web2 --bootstrap-version '11.4.0' --run-list 'role[base],role[web_server]'

## Tools server

    knife ec2 server create --image 'ami-ce7b6fba' --flavor m1.small --region 'eu-west-1' --ebs-size 40 --ebs-no-delete-on-term --server-connect-attribute private_ip_address --ssh-gateway zoz@gateway.aws.loyaltylion.com --ssh-user ubuntu --identity-file ~/.ssh/dave-chef-ec2.pem --subnet 'subnet-8d035ae5' --security-group-ids sg-9ae304f5,sg-7e907711 --environment production --node-name tools --run-list 'role[base],role[tools_server]'
    
    knife bootstrap ec2-54-229-3-159.eu-west-1.compute.amazonaws.com --ssh-user ubuntu --sudo --identity-file ~/.ssh/dave-chef-ec2.pem --environment production --node-name tools --run-list 'role[base],role[tools_server]'

## Log server

    knife ec2 server create --image 'ami-ce7b6fba' --flavor m1.medium --region 'eu-west-1' --ebs-size 40 --ebs-no-delete-on-term --server-connect-attribute private_ip_address --ssh-gateway zoz@gateway.aws.loyaltylion.com --ssh-user ubuntu --identity-file ~/.ssh/dave-chef-ec2.pem --subnet 'subnet-8d035ae5' --security-group-ids sg-9ae304f5,sg-7e907711 --environment production --node-name log --run-list 'role[base],role[log_server]'

    knife bootstrap 54.194.153.88 --ssh-user ubuntu --sudo --identity-file ~/.ssh/dave-chef-ec2.pem --environment production --node-name log --bootstrap-version '11.4.0' --run-list 'role[base],role[log_server]'

## monitoring server

    knife ec2 server create --image 'ami-ce7b6fba' --flavor t1.micro --region 'eu-west-1' --ebs-size 20 --ebs-no-delete-on-term --server-connect-attribute private_ip_address --ssh-gateway zoz@tools.loyaltylion.com --ssh-user ubuntu --identity-file ~/.ssh/dave-chef-ec2.pem --subnet 'subnet-62045d0a' --security-group-ids sg-9ae304f5 --environment production --node-name monitoring --run-list 'role[base],role[monitoring_server]'

## postgresql primary

    knife ec2 server create --image 'ami-fc7a6e88' --flavor m1.medium --region 'eu-west-1' --ebs-no-delete-on-term --server-connect-attribute private_ip_address --ssh-gateway zoz@tools.loyaltylion.com --ssh-user ubuntu --identity-file ~/.ssh/dave-chef-ec2.pem --subnet 'subnet-c3055cab' --security-group-ids sg-12bd5a7d,sg-c9ba59a6,sg-66907709 --environment production --node-name postgres1 --run-list 'role[base],role[postgresql_server],role[postgresql_primary]'

    knife bootstrap 192.168.10.77 --ssh-gateway zoz@tools.loyaltylion.com --sudo --ssh-user ubuntu --identity-file ~/.ssh/dave-chef-ec2.pem --environment production --node-name postgres1 --run-list 'role[base],role[postgresql_server],role[postgresql_primary]'

## postgresql standby

    knife ec2 server create --image 'ami-fc7a6e88' --flavor m1.small --region 'eu-west-1' --ebs-no-delete-on-term --server-connect-attribute private_ip_address --ssh-gateway zoz@tools.loyaltylion.com --ssh-user ubuntu --identity-file ~/.ssh/dave-chef-ec2.pem --subnet 'subnet-c3055cab' --security-group-ids sg-12bd5a7d,sg-c9ba59a6,sg-66907709 --environment production --node-name postgres2 --run-list 'role[base],role[postgresql_server],role[postgresql_standby]'

    knife bootstrap 192.168.11.227 --ssh-gateway zoz@tools.loyaltylion.com --server-connect-attribute private_ip_address --ssh-user ubuntu --identity-file ~/.ssh/dave-chef-ec2.pem --environment production --node-name postgres2 --run-list 'role[base],role[postgresql_server],role[postgresql_standby]'


# Staging VPC instances

## web server

    knife bootstrap 54.194.20.151 --sudo --ssh-user ubuntu --identity-file ~/.ssh/dave-chef-ec2.pem --environment staging --node-name staging-web1 --bootstrap-version '11.4.0' --run-list 'role[base],role[web_server]'

## app server

    knife bootstrap 54.194.179.143 --sudo --ssh-user ubuntu --identity-file ~/.ssh/dave-chef-ec2.pem --environment staging --node-name staging-app1 --run-list 'role[base],role[app_server]'

## postgres server

    knife bootstrap 10.0.1.4 --ssh-gateway zoz@stage.loyaltylion.com --ssh-user ubuntu --identity-file ~/.ssh/dave-chef-ec2.pem --environment staging --node-name staging-postgres1 --run-list 'role[base],role[postgresql_server],role[postgresql_primary]'






