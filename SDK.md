# Overview of SDK functionality

## data attributes

Loyli uses data attributes to inject functionality and data into the page. These all start with 'data-lion-':

* data-lion-referral
    * applied to an element, we will add a click event to it to bring up the referral modal. a value can be provided which is the page to refer to (not implemented yet)