# Postgres replication

We have two servers: `postgres1` and `postgres2`

`postgres1` is configured as a master, and `postgres2` as a hot standby.

## replication start

assuming the standby needs to be synced with the master (e.g. it's new, or was offline for a while)

    /etc/init.d/postgresql stop
    sudo -u postgres cp /data/postgresql/recovery.conf /tmp/recovery.conf
    rm -rf /data/postgresql/*
    sudo -u postgres pg_basebackup -v -P -c fast -h 192.168.10.38 -U replicator -D /data/postgresql
    sudo -u postgres cp /tmp/recovery.conf /data/postgresql/recovery.conf
    /etc/init.d/postgresql start

This will wipe out the old data and backup a new copy from the master into it

everything else should be taken care of by the chef configs once the standby server comes online