## Events

```
#!javascript
{
  id: ObjectId()
  
  // (index) event name, $ means a special, prizgo event, which are...
  // $pageview, $signup, $purchase
  name: '$pageview',
  time: ISODate(),

  // (index) reference to the site this event belongs to
  site_id: '28',

  // an event must be linked to at least a visitor or a customer. visitors are
  // weak links, connected only by a cookie on the client side, but a customer
  // is a strong link as it is sent by users when they are logged in.
  //
  // In certain cases a visitor_id will not be set (e.g. if an event is sent
  // by the server) but if a customer_id is present this will always be used
  visitor_id: '84123-48fjw-48283....',
  customer_id: ObjectId(),

  // it's possible for some pages to be identified as 'product' pages, if this
  // is true we'll link this event to said Product to help analytics
  product_id: ObjectId(),

  // if an event is sent to us directly by the merchant, this will be true. It's
  // useful to know which events are considered 'trusted'
  merchant: true

  // this contains properties specific to the event (in this case, a pageview)
  properties: {
    $page: 'http://spree.dev/signup'
  },

  // this contains all the referral information for this event. The URL and domain are only
  // provided if this was sent via javascript. Right now we only track initial_ (i.e. the
  // referrer information we grabbed when we first set the cookie for that user), but we could
  // also stack referrer information if that would be useful (i.e. for the lifetime of the
  // cookie, we track every referrer the person comes from)
  ref: {
    code: 'abc1',
    // we don't technically need the customer, as the code is enough, but this will allow quickly
    // connecting an event to a referring customer without a trip to the referrals collection
    customer: ObjectId(),
    initial_url: 'http://google.com/?s=something&b=ajeiu3',
  },

  // this is context data representing the state of the browser when this event was tracked
  // (for client-sent events, not applicable for server events)
  context: {
    user_agent: '',
    resolution: { x: 1024, y: 768 },
    viewport: { x: 900, y: 500 }
  }
}
```

## Customers

```
#!javascript
{
  id: ObjectId(...),
  site_id: '28', // (index) site this customer belongs to
  ext_id: '25' // (index) this is how they're connected to the Site
  email: 'me@clarkdave.net' // (index) email addr
  referred_by: ObjectId(..) // reference to another Customer
  properties: {
    // this contains anything else the site decided to tell us about
    // this user, name, settings, whatever
    name: 'Dave Clark'
    purchases: 25
  }
  
  points: {
    // available points can be spent at this exact moment
    approved: 100
    // pending points have been allocated but cannot be spent until they are confirmed
    // e.g. points from purchase may only be confirmed after 2 weeks have passed
    pending: 50
  }
}
```

## Referrals (collection: referrals)

```
#!javascript
{
  id: 'abc', // Unique Base62 ID, generated from a counter sequence
  site_id: '28', // site this referral belongs to (might not be needed)
  customer_id: ObjectId(..), // customer who created this referral
  url: 'http://prizgo.com', // url this referral links to
  // each referral has a 'location', aka type, which is where that url started life
  // there are four types right now: d, f, t, e
  // d is direct, so the link the customer copies and shares themselves
  // f & t are fb and twitter, which are shared to those sites
  // e is the code that is sent when a customer emails it
  // by tracking hits to each of these we can reasonably show which share location
  // each time someone hits the link we track the time and referrer and share location
  loc: 'd',
  hits: [
    { t: ISODate(..), r: 'google.com' }
  ],
  hits_n: 5 // count of hits, for quick query purposes
  // we track when a referral is 'shared' - tweeted, shared on fb, or emailed
  // we simply track the time the share happened, not where, since each code is
  // tied to a location anyway we can be reasonably sure it was shared there
  shares: [ ISODate(), ISODate(), ISODate(), ... ],
  shares_n: 3
}
```

## Point payouts, transactions, activities and rewards

A customer document contains its current points balance, a list of activities it has completed, rewards it has bought, and every points transaction that has taken place.

This is all stored within the customer document because:

* mongodb doesn't have transactions, which would make any points-related action considerably more complex, because we would have multiple collections which need updating atomically (locking, two phase commits, etc)
* we don't have any real need for a list of completed activities / earnt rewards in their own collection, and if we need it for analytics we can just create lists as needed from the customer docs
* even with these lists in place, a customer is very unlikely to ever hit the 16MB limit for a single document - that would be 10,000s of activities/rewards. If they ever come close, we can archive old list items
* mongodb has a comprehensive set of operators which let us do everything we need to inside a single document

### Customer document, with points/activities/rewards/transactions

```
#!javascript
{
  ...,

  points: {
    approved: 100,
    pending: 0
  },

  // any activities completed by this user will be added here. These all reference a document
  // from the 'activities' collection, and should have their state adjusted as the activity
  // changes over time. A new, or modified, activity will usually result in a change in points,
  // which will be applied immediately and logged in the transaction_log
  // 
  // the nature of a state change reflects how points are changed too:
  // (p) -> (a) = pending points removed, added as approved points
  // (p) -> (d) = pending points removed
  // (a) -> (v) = approved points removed, can result in negative points
  activities: [
    {
      t: Date(),
      activity_id: ObjectID(...),
      // this is the value (points awarded), which is tied to the activity_id
      value: 100
      // (p)ending, (a)pproved, (d)eclined, (v)oided
      state: 'p',
      // some activities have a '<something>_id', which is an id that is a link back to the
      // merchant, allowing this activity to be updated over time (i.e. product returns, review
      // confirmations, etc)
      // the actual field name depends on the activity, it could be order_id, review_id, etc
      // generally speaking, this field should be unique, so there should not be two activities
      // in here with the same review_id, or order_id
      order_id: 'xyz.54235D'
    }
  ],

  // any rewards claimed by this customer will be added here
  rewards: [
    {
      t: Date(),
      reward_id: ObjectID(...),
      // the cost of this reward
      cost: 100,
      // (a)pproved, (r)efunded
      state: 'a',
      // some rewards have contextual data. Discount vouchers, for example, have a code
      // and must also keep track of it they have
      context: {
        code: 'EAFE.3025',
        used: false
      }
    }
  ],

  // transaction log. Any time the points value of the user is adjusted, it must 
  // be logged here. Once a transaction is added to the log it should never be changed
  // or removed; it should be possible to sum up the pending and approved values
  // in this array to arrive at their current points total
  transaction_log: [
    {
      t: Date(),
      p: 100,
      a: 0
    },
    {
      t: Date(),
      p: -100,
      a: 100
    }
  ]
}
```


### Adjusting the state of an activity/reward

Some things start as pending. Their first transaction will be a pending transaction, and points will be added to the customer's points_pending total.

Each adjustable action has a reward_id/activity_id and a <something>_id. Between these two fields we can allow for either automated or triggered adjustment. For activities, the merchant defines if an activity starts as pending, and if so how it should be approved. Let's see some examples:

**Purchase activity**: starts pending; becomes approved automatically after 2 weeks

The user completes a purchase. We check if there is a corresponding Activity (there is) and create a new Action, linking it with the purchase activity_id and setting its thing_id to the order ID provided to us by the merchant. A single, pending transaction is added with the new Action, and the customer's pending points total is updated.

Now one of two things can happen: 

* The merchant can tell us that the order has been cancelled. We will look up an Action with this thing_id and then add a new transaction with state 'declined', and remove the pending points. We'll change the state of the Action to 'declined', and it will no longer be looked at when checking for Actions to approve.
* 2 weeks pass, and the Action is eligible for approval automatically. We have a background job running which continuously looks for Actions which are linked to an Activity whose approval criteria is time-based, and checks if those Actions are eligible (e.g. their Date is sufficiently in the past). Any eligible Actions will have a new Approved transaction added, etc.
* **ALTERNATE** When the Action is added, if the linked Activity is time-based, we add this to the Action as the approve_on field. We have a job which simply looks for non-approved actions with an approve_on field, and if the current time is > the approve_on date, they are approved.

Later, if this order was returned, the merchant can tell us using the order ID. We'll find the associated Action (by thing_id) and add a new 'Voided' transaction and remove points from the customer (hopefully they didn't spend them already!)

**Referral activity**: starts pending; becomes approved when its trigger purchase is approved

This is a bit different from others. A referral action is created automatically when the criteria specified by the merchant are hit (usually when a new customer makes their first purchase). In this case, the referral action starts as pending. It is only approved when the triggering Action (e.g. a purchase) is also approved. When this happens, we'll check for any dependent Actions and approve them too.

### Action constraints

Activities can be limited in how many times they can be completed (per day, week, etc). At any point when an activity is completed, and an Action is about to be created, we check for any Actions over the last <day/week> and if any are found, which preclude the new Action, we won't add it (and will return a sensible error, such that the user can be told?)


## Activities

These are the activities which provide points, set up per-site. When a user completes one of these activities (for the active ones, at least) they'll get points.

### Activity types

* facebook like. Passive, awards points over time. Requires FB connected first
* twitter follow. " ". Requires Twitter connected first
* tweet. Generic tweet, usually limited to once a day. Doesn't technically required T connect, but should?
* tweet purchase. Specific tweet done post-purchase?
* write review. Leave a review!
* ... LOADS more ...

.
```
#!javascript
{
  id: ObjectID(...)
  site_id: 2
  // types are known identifiers, of which there will be lots
  name: <purchase | fb_like | tweet | twitter_follow | review | referral | etc>

  // points awarded for completing this activity. Purchases are a special case -- this value will
  // be the per euro/gbp/dollar reward, and will be calculated at the moment the Action is created
  value: 5
  
  // some activities need to be triggered by another activity, i.e. a referral activity
  // triggered by a purchase. This should be the ID of the triggering activity. Any time this activity
  // gets created, any activities with the right triggered_by id will also (if...
  triggered_by: ObjectID(...)
  
  // allow this activity to track another activity. When an activity with this field is created, its
  // state will automatically follow that of its tracker activity (so it will be approved when its
  // master is approved, declined when... etc)
  master: ObjectID(...)

  // activities can start pending or approved; if pending, it will need to be approved
  // somehow. If approved, it will award the points immediately and to remove them it would
  // have to be voided later on
  initial_state: (p)ending or (a)pproved 
  
  // how many times a day this can be performed whilst still giving points; 0 for unlimited
  daily_limit: 0
  
  // if the initial state is pending, an approval method must be specified. this is an object
  // where (m)ethod is a known identifier (time, or triggered) and the rest of the object
  // contains specific criteria for that method
  approval_method: {
    m: 'time'
    when: '342347238434' // time in seconds until this is approved
  }
  
  // alternative:
  approval_method: {
    m: 'triggered'
    on: 'manual' // wait for a trigger from the merchant (using the Action's thing_id)
  }
  
  // alternative:
  approval_method: {
    m: ' 
  }
}
```

## Rewards

These are basically the opposite of activities. Pre-defined types with a cost.

### Reward types

* discount
* more later?

.
```
#!javascript
{
  id: ObjectID(...)
  site_id: 2
  // this is the cost of the reward
  cost: 200
  name: $discount
  // the properties object depends on the type of reward
  properties: {
    // all $discount rewards must have a method, which determines how it is given out
    // at the moment this must always be 'voucher'
    method: 'voucher',
    // 
    available_codes: ['abc123', '123abc', 'def123', '123def', ...],
    used_codes: ['abcabc']
  }
}
```