# rewards page

## individual reward

**variables:**
    
    // true if the customer has enough points for this reward
    customer_can_afford: boolean
    
this template is a bit tricky because it has a lot of extra bits

**the redemption process:**
 
when the element `[data-part='redeem-btn']` is clicked, we will add the class `.visible` to the `[data-part='redeem-confirm']` element.

Inside the `redeem-confirm` element should be two buttons, `[data-part='redeem-confirm-yes']` and `[data-part='redeem-confirm-no']`

When either of these is clicked the `.visible` class will be removed on the `redeem-confirm` element.

In addition, if `redeem-confirm-yes` was clicked then a reward will be claimed (if possible) which, if successful will trigger the following:

* a new `redeem_reward_claimed` template will be created and added to the page, which is intended to be shown as a modal but could be placed anywhere. this will contain details of the reward just claimed
* a `claimed_reward` template will be added to the element `[data-part='claimed-rewards']`
