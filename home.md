# Prizgo wiki

* [Data models](Data models)
* [Customer authentication](Customer authentication)
* [Messages and operations](Messages and operations)
* [SDK](SDK)
* [Server setup](Server setup)
* [Integration](Integration)
