The account pages are where a customer interacts with the loyalty program. They can:

* View their balance and transaction history
* Find out how to earn more points
* Spend points on rewards
* Refer friends
* Manage account settings

# Integration

How does a merchant get the account pages on their site? They use our iframes (though they don't add them directly).

There are two ways this can work:

## Single iframe

The 'account pages' are encapsulated within a single iframe. This has multiple pages within it for each account action. The merchant just needs to have one container (parent) page on their site to stick this iframe in and their job is done.

**Benefits**:
* less effort for merchant to set up (just one page and done)
* we have maximum flexibility in changing the content within the whole iframe (changing the pages, layout, etc)
* we can use backbone for rendering the whole thing (so very quick, no reloads between pages)

**Drawbacks**:
* trickier for a merchant to link to a specific 'page'
* may feel less 'integrated' into the merchant's site
* more templates to manage and maintain (tabs, etc)

## Multiple iframes (modular)

The merchant creates their own urls, like `/loyalty/account/home`, `/loyalty/account/earn`. On each one the `data-lion-account` attribute is given a value which tells it to load a specific page, e.g. `data-lion-account='home'`

This can be thought of as a 'modular' approach, where each 'page' is more like a 'module' that can be plugged in to an appropriate location. For example, the merchant could put the 'transactions' module onto their main account page for a customer, under a heading like 'Recent points activity'. 

**Benefits**:
* fewer templates to manage - really just a 'page' template and a few specific elements
* merchant has more freedom in where the 'page' appears and flows around their existing UI. For example, the loyalty 'pages' could be a submenu, or popup menu, in an existing account UI. The 'modules' can also be plugged in anywhere, and do not necessarily have to all live together.

**Drawbacks**:
* not so plug-and-play for the merchant - they need to set up more pages (or one page with more logic), menus, etc.
* navigating between pages will be more resource intensive, as each time will reload both the parent page and the iframe.
 * it's possible this could be reduced if the account pages were using client-side mvc, if we had the appropriate hooks in the LL SDK. But I doubt that's going to happen with much regularity.

## Both

The right answer is probably to offer both. For merchants who want a very unique experience, they can use the modular approach, and customise each module to match their existing UI style. And for those merchants who just want to get going quickly, they can just create an all-encompassing 'Loyalty account' page and stick in the single iframe and let us take care of everything.














